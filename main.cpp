#include <omp.h>
#include <iostream>
#include <tgmath.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    int count = 8000000;
    double *vector = new double[count];
    #pragma omp parallel for
    for (int i = 0; i < count; i++) {
        vector[i] = rand()%20;
    }
    double t1Mono = omp_get_wtime();
    double sum = 0;
    for (int i = 0; i < count; i++) {
        sum += vector[i] * vector[i];
    }
    sum = sqrt(sum);
    double t2mono = omp_get_wtime();
    std::cout << "В один поток программа работает: " << t2mono - t1Mono << std::endl << "С результатом:" << sum << std::endl;

    double t1Parallel = omp_get_wtime();
    sum = 0;
    #pragma omp parallel for
    for (int i = 0; i < count; i++) {
        vector[i]*=vector[i];
    }
    #pragma omp parallel for reduction(+:sum)
    for (int i = 0; i < count; i++) {
        sum+=vector[i];
    }
    sum = sqrt(sum);
    double t2Parallel = omp_get_wtime();
    std::cout << "Через openMP программа работает: " << t2Parallel - t1Parallel << std::endl << "С результатом:" << sum << std::endl;
    return 0;
}