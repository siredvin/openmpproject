#include <omp.h>
#include <iostream>
#include <tgmath.h>
#include <stdlib.h>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>

int main(int argc, char *argv[]) {
    unsigned count = 10;
    using namespace boost::numeric::ublas;
    matrix<double> m(count, count);
    vector<double> v(count);
    do {
            for (unsigned i=0;i<count;i++){
                for (unsigned j = 0; j < count; j++) {
                    m(i,j) = rand()%20;
                }
                v(i) = rand()%20;
            }
    } while (norm_1(m)==0);
    std::cout<< m<< std::endl << v <<std::endl;
    //Прямий хід
    for (unsigned columnIndex = 0; columnIndex <count; columnIndex++){
        double el = m(columnIndex, columnIndex);
        if (el!=0){
            for (unsigned divisionIndex = columnIndex;divisionIndex<count;divisionIndex++){
                m(columnIndex,divisionIndex)/=el;
            }
            v(columnIndex)/=el;
            #pragma omp parallel for
            for (unsigned rowIndex = columnIndex+1; rowIndex <count; rowIndex++){
                double k = m(rowIndex,columnIndex);
                v(rowIndex)-=v(columnIndex)*k;
                for (unsigned selfColumnIndex=columnIndex;selfColumnIndex<count;selfColumnIndex++){
                    m(rowIndex,selfColumnIndex)-= m(columnIndex,selfColumnIndex)*k;
                }
            }
        }
    }
    //std::cout << m;
    //Зворотній хід
    for (unsigned columnIndex = count-1;columnIndex>0;columnIndex--){
        #pragma omp parallel for
        for (unsigned rowIndex=0;rowIndex<columnIndex;rowIndex++){
            v(rowIndex)-=v(columnIndex)*m(rowIndex,columnIndex);
            m(rowIndex,columnIndex) = 0;
        }
    }
    std::cout<< m << std::endl << v << std::endl;
    return 0;
}